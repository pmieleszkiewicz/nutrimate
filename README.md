# NutriMate

Nutrition assistant written in Laravel 5.7, VueJS and Bootstrap 4.
Admin panel is a multi page application (Blade templating engine).
Main app is a single page application written in VueJS.

## Live demo

[Live demo](http://pmieleszkiewicz-nutrimate.herokuapp.com)


### Sample credentials

Email: user@nutrimate.com	
Password: user123

Email: admin@nutrimate.com  
Password: admin123