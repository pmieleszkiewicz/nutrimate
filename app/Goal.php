<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $fillable = ['calories', 'proteins', 'carbs', 'fats', 'user_id'];
}
