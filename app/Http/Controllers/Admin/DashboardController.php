<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $newUsers = User::orderBy('created_at', 'desc')->take(5)->get();
        $lastLoggedUsers = User::with('role')
            ->whereNotNull('last_login_at')
            ->orderBy('last_login_at', 'desc')
            ->take(10)
            ->get();
        $newProducts = Product::orderBy('proposed_at', 'desc')
            ->whereNotNull('accepted_by')
            ->with('creator')
            ->take(5)
            ->get();


        $measurementsNumber = DB::table('measurements')->count();
        $usersNumber = DB::table('users')->count();
        $productsNumber = DB::table('products')->count();

        return view('admin.dashboard.index', [
            'users' => $newUsers,
            'products' => $newProducts,
            'lastLoggedUsers' => $lastLoggedUsers,
            'usersNumber' => $usersNumber,
            'productsNumber' => $productsNumber,
            'measurementsNumber' => $measurementsNumber
        ]);
    }
}
