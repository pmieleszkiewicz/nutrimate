<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductEditRequest;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'proposed' => 'boolean'
        ]);

        $search = $request->query('search');
        $proposed = $request->query('proposed');

        if ($proposed) {
            $products = Product::whereNotNull('proposed_at')->whereNull('accepted_by');
        } else {
            $products = Product::whereNotNull('accepted_by');
        }

        $products = $products
            ->where('name', 'like', "%{$search}%")
            ->orWhere('id', (int) $search);
        $products = $products->with(['creator', 'userWhoAccepted'])->orderBy('proposed_at', 'DESC')->paginate(10);

//        return $products;
        return view('admin.products.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        //
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//    public function store(Request $request)
//    {
//        //
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.products.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('admin.products.edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductEditRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->name = $request['name'];
        $product->proteins = $request['proteins'];
        $product->carbs = $request['carbs'];
        $product->fats = $request['fats'];
        if ($request['isGlobal']) {
            $product->accepted_by = Auth::id();
        } else {
            $product->accepted_by = null;
        }
        $product->save();

        $request->session()->flash('status', "Produkt {$product->name} zaktualizowano!");
        return view('admin.products.show', ['product' => $product]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function accept(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        if ($product->accepted_by) {
            $product->accepted_by = null;
        } else {
            $product->accepted_by = Auth::id();
        }
        $product->save();
        $request->session()->flash('status', "Produkt {$product->name} dodany do globalnej puli!");

        return back();
    }
}
