<?php

namespace App\Http\Controllers\Admin;

use App\Measurement;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatsController extends Controller
{
    public function index()
    {
        $usersLastWeek = User::where('created_at', '>=', new \DateTime('-1 week'))->count();
        $usersLastMonth = User::where('created_at', '>=', new \DateTime('-1 month'))->count();
        $users = User::all()->count();

        $productsLastWeek = Product::where('created_at', '>=', new \DateTime('-1 week'))->count();
        $productsByUsers = Product::where([
            ['proposed_at', '!=', null],
            ['accepted_by', '!=', null]
        ])->count();
        $products = Product::all()->count();

        $measurementsLastWeek = Measurement::where('created_at', '>=', new \DateTime('-1 week'))->count();
        $measurementsLastMonth = Measurement::where('created_at', '>=', new \DateTime('-1 month'))->count();
        $measurements = Measurement::all()->count();



        return view('admin.stats.index', [
            'users' => $users,
            'usersLastWeek' => $usersLastWeek,
            'usersLastMonth' => $usersLastMonth,

            'products' => $products,
            'productsLastWeek' => $productsLastWeek,
            'productsByUsers' => $productsByUsers,

            'measurements' => $measurements,
            'measurementsLastWeek' => $measurementsLastWeek,
            'measurementsLastMonth' => $measurementsLastMonth
        ]);
    }
}
