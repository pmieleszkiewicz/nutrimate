<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    const ORDER_BY_FIELDS = ['id', 'name', 'email'];

    public function __construct()
    {
        $this->middleware('role:admin', ['only' => ['update']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($search = $request->query('search')) {
//            $users = User::whereHas('role', function ($q) use ($search) {
//                    $q->where('name', 'like', "%{$search}%");
//                });
            $users = User::where('name', 'like', "%{$search}%")
                ->orWhere('email', 'like', "%{$search}%")
                ->orWhere('id', (int) $search)
                ->orWhereHas('role', function ($q) use ($search) {
                    $q->where('name', 'like', "%{$search}%");
                });
        } else {
            $users = User::query();
        }

        $orderBy = $request->query('orderBy') ?? 'id';
        $order = $request->query('order') ?? 'asc';

        if (in_array($orderBy, self::ORDER_BY_FIELDS)) {
            $users = $users->orderBy($orderBy, $order);
        }

        $users = $users->with('role')->paginate(10);
        return view('admin.user.index', ['users' => $users]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $user = User::findOrFail($id)->with(['roles' => function ($query) {
//            $query->select('name');
//        }])->get();

        $user = User::with('role')->findOrFail($id);
        return view('admin.user.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->blocked = !$user->blocked;
        if ($user->blocked) {
            $user->blocked_at = date('Y-m-d H:i:s');
        } else {
            $user->blocked_at = null;
        }
        $user->save();
        return back();
    }

    /**
     * Returns page with products added by direct user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function products($id)
    {
        $user = User::findOrFail($id);
        $products = Product::with('userWhoAccepted')
            ->where('user_id', $id)
            ->whereNotNull('accepted_by')
            ->orderBy('id', 'desc')
            ->paginate(10);

        return view('admin.user.show', ['user' => $user, 'products' => $products]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
