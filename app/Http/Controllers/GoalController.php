<?php

namespace App\Http\Controllers;

use App\Goal;
use App\User;
use Illuminate\Http\Request;

class GoalController extends Controller
{
    public function store(Request $request, $id)
    {
        $goal = Goal::where('user_id', $id)->first();

        // Edit
        if ($goal) {
            $goal->calories = $request['calories'];
            $goal->proteins = $request['proteins'];
            $goal->carbs = $request['carbs'];
            $goal->fats = $request['fats'];
            $goal->save();

            return $goal;
        }
        $request['user_id'] = $id;
        $goal = Goal::create($request->all());

        return $goal;
    }
}
