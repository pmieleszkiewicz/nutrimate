<?php

namespace App\Http\Controllers;

use App\Http\Requests\MealsGetRequest;
use App\Meal;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class MealController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only(['store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(MealsGetRequest $request)
    {
        $user = Auth::guard('api')->user();

        $meals = Meal::with('products')->where('user_id', $user->id);
        if ($request->has('date')) {
            $searchDate = new \DateTime($request->date);
            $meals = $meals->whereDate('created_at', $searchDate);
        }

        return $meals->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::guard('api')->user();

        $meal = Meal::create([
            'name' => 'Nowy posiłek',
            'user_id' => $user->id
        ]);

        return Meal::with('products')->findOrFail($meal->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $meal = Meal::with('products')->findOrFail($id);

        /* update only meal name */
        if ($request->exists('mealName')) {
            $meal->name = $request->mealName;
            $meal->save();
            return $meal;
        }

        $meal->products()->detach();
        /* it could be also done with attach*/
        $products = $request->products;
        foreach ($products as $product) {
            DB::table('meal_product')->insert([
                'meal_id' => $id,
                'product_id' => $product['id'],
                'weight' => $product['weight']
            ]);
        }
        return Meal::with('products')->findOrFail($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $meal = Meal::findOrFail($id);
        $meal->delete();

        return $meal;
    }

    public function pdf()
    {
        $meals = Meal::where('user_id', 1)->with('products')->get();
        $pdf = PDF::loadView('pdf.document', [
            'meals' => $meals
        ]);
        return $pdf->stream('document.pdf');
    }
}
