<?php

namespace App\Http\Controllers;

use App\Measurement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MeasurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $startDate = $request->query('startDate');
        $user = $request->user();
        if ($startDate !== null) {
            $startDate = new \DateTime($startDate);
            return $user->measurements()->where('created_at', '>=', $startDate)->orderBy('created_at', 'desc')->get();
        }
        return $user->measurements()->orderBy('created_at', 'desc')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $measurement = Measurement::create([
            'weight' => $request['weight'],
            'height' => $request['height'],
            'body_fat' => $request['body_fat'],
            'biceps' => $request['biceps'],
            'chest' => $request['chest'],
            'waist' => $request['waist'],
            'thigh' => $request['thigh'],
            'user_id' => $request->user()->id
        ]);

        return $measurement;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = $request->user();
        $measurement = Measurement::where([
            'user_id' => $user->id,
            'id' => $id
        ])->firstOrFail();

        return $measurement;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = $request->user();
        $measurement = Measurement::findOrFail($id);
        $creator = $measurement->user;

        if ($user->id !== $creator->id) {
            return response()->json(['message' => 'Not authorized'], 401);
        }

        $measurement->delete();
        return response()->json(["message" => "Measurement of id: {$measurement->id} deleted"], 200);
    }
}
