<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        $search = $request->get('search');
        if ($search) {
            $products = Product::where([
                ['name', 'like', "%{$search}%"],
                ['user_id', $user->id]
            ])->orWhere([
                ['name', 'like', "%{$search}%"],
                ['user_id', '!=', $user->id],
                ['accepted_by', '!=', 'null']
            ])->take(5)->get();
        } else {
            $products = Product::all();
        }
        return $products;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'proteins' => 'required|numeric|min:0',
            'carbs' => 'required|numeric|min:0',
            'fats' => 'required|numeric|min:0',
            'calories' => 'required|numeric|min:0',
        ]);

        $product = Product::create([
            'name' => $request['name'],
            'proteins' => $request['proteins'],
            'carbs' => $request['carbs'],
            'fats' => $request['fats'],
            'calories' => $request['calories'],
            'user_id' => $request->user()->id
        ]);

        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $user = $request->user();
        $isOwnerOfProduct = $user->products()->where('id', '=', $id)->exists();
        if (!$isOwnerOfProduct) {
            return response()->json(['message' => 'Not authorized'], 401);
        }
        $product = Product::findOrFail($id);

        return $product;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3',
            'proteins' => 'required|numeric',
            'carbs' => 'required|numeric',
            'fats' => 'required|numeric',
            'calories' => 'required|numeric',
        ]);
        $product = Product::findOrFail($id);

        $product->name = $request->name;
        $product->proteins = $request->proteins;
        $product->carbs = $request->carbs;
        $product->fats = $request->fats;
        $product->calories = $request->calories;

        $product->save();

        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $user = $request->user();
        $product = Product::findOrFail($id);
        $creator = $product->creator;

        if ($user->id !== $creator->id) {
            return response()->json(['message' => 'Not authorized'], 401);
        }

        $product->delete();
        return response()->json(["message" => "Product of id: {$product->id} deleted"], 200);
    }

    public function proposeProduct($id, Request $request)
    {
        $user = $request->user();
        $product = $user->products()->findOrFail($id);

        $product->proposed_at = new \DateTime();
        $product->save();
        return response()->json(["message" => "Product has been proposed"], 200);
    }
}
