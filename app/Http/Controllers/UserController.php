<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserUpdateSettings;
use App\Http\Resources\ProductCollection;
use App\Product;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Update user settings.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateSettings(UserUpdateSettings $request)
    {
        $user = $request->user();
        $settings = $user->settings;
        $settings->theme = $request->theme;
        $settings->show_numbers_in_bars = $request->show_numbers_in_bars;
        $settings->number_of_items_in_table = $request->number_of_items_in_table;

        $settings->save();

        return $settings;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return products which belong to user
     * @param Request $request
     * @return ProductCollection
     */
    public function products(Request $request)
    {
        $user = $request->user();

        return new ProductCollection($user->products()->whereNull('accepted_by')->get());
    }
}
