<?php

namespace App\Http\Middleware;

use Closure;

class CheckProductOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        $productId = $request->route('id');
        return $next($request);
    }
}
