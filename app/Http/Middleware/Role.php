<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $user = $request->user();
        $roles = explode('|', $roles);

        if (!$user->hasRole($roles)) {
            abort(403, 'Access denied');
        }

        return $next($request);
    }
}
