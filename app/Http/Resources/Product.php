<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "proteins" => $this->proteins,
            "carbs" => $this->carbs,
            "sugars" => $this->sugars,
            "fats" => $this->fats,
            "calories" => $this->calories,
        ];
    }
}
