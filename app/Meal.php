<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meal extends Model
{
    protected $fillable = ['name', 'user_id'];
    /* Relations */
    // Users
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Products
    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('weight');
    }
}
