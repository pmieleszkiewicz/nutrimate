<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name', 'proteins', 'carbs', 'fats', 'calories', 'user_id',
    ];

    protected $dates = [
            'proposed_at'
    ];

    /* Relations */
    // User
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function userWhoAccepted()
    {
        return $this->belongsTo(User::class, 'accepted_by');
    }

    // Product
    public function meals()
    {
        return $this->belongsToMany(Meal::class)->withPivot('weight');
    }
}
