<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'theme', 'show_numbers_in_bars', 'number_of_items_in_table',
    ];

    protected $casts = [
        'theme' => 'boolean',
        'show_numbers_in_bars' => 'boolean',
        'number_of_items_in_table' => 'int'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
