<?php

namespace App;

use App\Notifications\ResetPassword;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

class User extends Authenticatable
{
    use Notifiable;

//    protected $with = ['role', 'settings'];

    /* use field in Blade with Carbon */
    protected $dates = [
        'last_login_at'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'api_token', 'gender',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /* Getters and helpers */
    public function isMale(): bool
    {
        return $this->gender === 'm' ? true : false;
    }

    public function hasRole($roles): bool
    {
        foreach ($roles as $role) {
            if ($this->role->name === $role)
                return true;
        }
        return false;
    }

    public function getThemeName($prefix = false): string
    {
        /* prefix needed for Bootstrap classes: active */
        if ($this->settings->theme) {
            return $prefix ? '-pink' : 'pink';
        } else {
            return $prefix ? '-primary' : 'primary';
        }
    }

    public function getThemeNameActive(): string
    {
        return $this->settings->theme ? '-pink' : '';
    }


    /* Relations */
    // Products
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function userAcceptedProducts()
    {
        return $this->hasMany(Product::class)
            ->whereNotNull('products.accepted_by');
    }

    public function acceptedByUser()
    {
        return $this->hasMany(Product::class, 'accepted_by');
    }

    // Meals
    public function meals()
    {
        return $this->hasMany(Meal::class);
    }

    // Roles
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    // Settings
    public function settings()
    {
        return $this->hasOne(Settings::class);
    }

    // Measurements
    public function measurements()
    {
        return $this->hasMany(Measurement::class);
    }

    // Goal
    public function goal() {
        return $this->hasOne(Goal::class);
    }

    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPassword($token));
    }

}
