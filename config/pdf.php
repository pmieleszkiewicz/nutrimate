<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => 'Nutrimate',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Nutrimate',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('../temp/')
];
