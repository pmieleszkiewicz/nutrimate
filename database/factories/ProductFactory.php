<?php

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    $carbs = $faker->randomFloat($nbMaxDecimals = 1, $min = 20, $max = 80);
    $isAccepted = rand(0, 5);

    return [
        'name' => $faker->word,
        'proteins' => $faker->randomFloat($nbMaxDecimals = 1, $min = 0, $max = 80),
        'carbs' => $carbs,
        'sugars' => $carbs - $faker->randomFloat($nbMaxDecimals = 1, $min = 0, $max = 10),
        'fats' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 80),
        'calories' => $faker->numberBetween($min = 100, $max = 800),
        'user_id' => $faker->numberBetween($min = 1, $max = 5),
        'created_at' => $faker->dateTimeBetween($startDate = '-10 days', $endDate = 'now', $timezone = null),
        'proposed_at' => !$isAccepted ? $faker->dateTimeBetween($startDate = '-20 days', $endDate = 'now', $timezone = null) : null,
        'accepted_by' => !$isAccepted ? 1 : null
    ];
});
