<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $gender = $faker->randomElement($array = ['f','m']);
    $name = $gender === 'm' ? "{$faker->firstNameMale()} {$faker->lastNameMale()}" : "{$faker->firstNameFemale()} {$faker->lastNameFemale()}";
    return [
        'name' => $name,
        'email' => str_replace('-', '', str_slug($name)) . '@example.com',
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'role_id' => 3,
        'date_of_birth' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'gender' => $gender,
        'api_token' => str_random(64)
    ];
});
