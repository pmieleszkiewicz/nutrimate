<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->float('weight');
            $table->float('height')->nullable();
            $table->float('body_fat')->nullable();
            $table->float('biceps')->nullable();
            $table->float('chest')->nullable();
            $table->float('waist')->nullable();
            $table->float('thigh')->nullable();
            $table->timestamps();
        });

        Schema::table('measurements', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('measurements');
    }
}
