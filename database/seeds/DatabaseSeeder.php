<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RolesTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(SettingsTableSeeder::class);
         $this->call(ProductTableSeeder::class);
         $this->call(MealsTableSeeder::class);
         $this->call(MeasurementsTableSeeder::class);
         $this->call(GoalsTableSeeder::class);
    }
}
