<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GoalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('goals')->insert([
            [
                'user_id' => 4,
                'calories' => 3000,
                'proteins' => 140,
                'carbs' => 450,
                'fats' => 70
            ]
        ]);
    }
}
