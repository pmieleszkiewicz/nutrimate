<?php

use App\Meal;
use App\Product;
use Illuminate\Database\Seeder;

class MealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    const mealSet1 = ['Śniadanie', 'Lunch', 'Obiad', 'Kolacja'];
    const mealSet2 = ['Śniadanie', 'Obiad', 'Po obiedzie', 'Kolacja', 'Przed snem'];

    public static function getMealSet(int $id)
    {
        if ($id === 1) {
            return self::mealSet1;
        } else {
            return self::mealSet2;
        }
    }

    public function run()
    {
        $mealSetId = rand(1,2);
        $mealSet = self::getMealSet($mealSetId);

        for ($i = 0; $i < count($mealSet); $i++) {
            $meal = Meal::create([
                'name' => $mealSet[$i],
                'user_id' => 1
            ]);
            $products = Product::inRandomOrder()->take(rand(3, 5))->pluck('id')->toArray();

            $weights = [20, 25, 50, 75, 80, 100, 130, 150];

            foreach ($products as $product) {
                $meal->products()->attach($product, ['weight'=> $weights[array_rand($weights)]]);
            }

            $meal->save();
        }

        $mealSetId = rand(1,2);
        $mealSet = self::getMealSet($mealSetId);

        for ($i = 0; $i < count($mealSet); $i++) {
            $meal = Meal::create([
                'name' => $mealSet[$i],
                'user_id' => 2
            ]);
            $products = Product::inRandomOrder()->take(rand(3, 5))->pluck('id')->toArray();

            $weights = [20, 25, 50, 75, 80, 100, 130, 150];

            foreach ($products as $product) {
                $meal->products()->attach($product, ['weight'=> $weights[array_rand($weights)]]);
            }

            $meal->save();
        }

        $mealSetId = rand(1,2);
        $mealSet = self::getMealSet($mealSetId);

        for ($i = 0; $i < count($mealSet); $i++) {
            $meal = Meal::create([
                'name' => $mealSet[$i],
                'user_id' => 3
            ]);
            $products = Product::inRandomOrder()->take(rand(3, 5))->pluck('id')->toArray();

            $weights = [20, 25, 50, 75, 80, 100, 130, 150];

            foreach ($products as $product) {
                $meal->products()->attach($product, ['weight'=> $weights[array_rand($weights)]]);
            }

            $meal->save();
        }



        for ($j = 1; $j <= 31; $j++) {
            $mealSetId = rand(1,2);
            $mealSet = self::getMealSet($mealSetId);

            for ($i = 0; $i < count($mealSet); $i++) {
                $meal = Meal::create([
                    'name' => $mealSet[$i],
                    'user_id' => 4,

                    'created_at' => '2018-12-' . ($j < 10 ? '0' : '') . $j
                ]);
                $products = Product::inRandomOrder()->take(rand(3, 5))->pluck('id')->toArray();

                $weights = [20, 25, 50, 75, 80, 100, 130, 150];

                foreach ($products as $product) {
                    $meal->products()->attach($product, ['weight'=> $weights[array_rand($weights)]]);
                }

                $meal->save();
            }
        }

        for ($j = 1; $j <= 10; $j++) {
            $mealSetId = rand(1,2);
            $mealSet = self::getMealSet($mealSetId);

            for ($i = 0; $i < count($mealSet); $i++) {
                $meal = Meal::create([
                    'name' => $mealSet[$i],
                    'user_id' => 4,

                    'created_at' => '2019-01-' . ($j < 10 ? '0' : '') . $j
                ]);
                $products = Product::inRandomOrder()->take(rand(3, 5))->pluck('id')->toArray();

                $weights = [20, 25, 50, 75, 80, 100, 130, 150];

                foreach ($products as $product) {
                    $meal->products()->attach($product, ['weight'=> $weights[array_rand($weights)]]);
                }

                $meal->save();
            }
        }
    }
}
