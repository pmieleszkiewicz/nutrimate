<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MeasurementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('measurements')->insert([
            [
                'user_id' => 1,
                'weight' => 75.0,
                'created_at' => new DateTime('2018-09-01')
            ],
            [
                'user_id' => 1,
                'weight' => 75.3,
                'created_at' => new DateTime('2018-09-08')
            ],
            [
                'user_id' => 1,
                'weight' => 75.4,
                'created_at' => new DateTime('2018-09-10')
            ],
            [
                'user_id' => 1,
                'weight' => 76,
                'created_at' => new DateTime('2018-09-15')
            ],
            [
                'user_id' => 1,
                'weight' => 76.5,
                'created_at' => new DateTime('2018-09-20')
            ],
            [
                'user_id' => 1,
                'weight' => 77.2,
                'created_at' => new DateTime('2018-09-23')
            ]
        ]);

        // admin
        DB::table('measurements')->insert([
            [
                'user_id' => 2,
                'weight' => 90.0,
                'body_fat' => 20.2,
                'created_at' => new DateTime('2018-09-01')
            ],
            [
                'user_id' => 2,
                'weight' => 89.5,
                'body_fat' => 20.0,
                'created_at' => new DateTime('2018-09-08')
            ],
            [
                'user_id' => 2,
                'weight' => 89,
                'body_fat' => 19.6,
                'created_at' => new DateTime('2018-09-16')
            ],
            [
                'user_id' => 2,
                'weight' => 88.4,
                'body_fat' => 19.2,
                'created_at' => new DateTime('2018-09-24')
            ],
            [
                'user_id' => 2,
                'weight' => 88,
                'body_fat' => 18.7,
                'created_at' => new DateTime('2018-10-01')
            ],
            [
                'user_id' => 2,
                'weight' => 87.5,
                'body_fat' => 18.1,
                'created_at' => new DateTime('2018-10-08')
            ],
            [
                'user_id' => 2,
                'weight' => 86.9,
                'body_fat' => 17.4,
                'created_at' => new DateTime('2018-10-20')
            ],
        ]);

        // moderator
        DB::table('measurements')->insert([
            [
                'user_id' => 3,
                'weight' => 60,
                'body_fat' => 10,
                'created_at' => new DateTime('2018-12-01')
            ],
            [
                'user_id' => 3,
                'weight' => 61,
                'body_fat' => 10.2,
                'created_at' => new DateTime('2018-12-08')
            ],
            [
                'user_id' => 3,
                'weight' => 61.7,
                'body_fat' => 10.3,
                'created_at' => new DateTime('2018-12-14')
            ],
        ]);

        // user
        DB::table('measurements')->insert([
            [
                'user_id' => 4,
                'weight' => 60,
                'body_fat' => 10,
                'height' => 173,
                'biceps' => 30.1,
                'chest' => 90,
                'waist' => 75,
                'thigh' => 52,
                'created_at' => new DateTime('2018-08-01')
            ],
            [
                'user_id' => 4,
                'weight' => 60.4,
                'body_fat' => 10.1,
                'height' => 173,
                'biceps' => 30.2,
                'chest' => 90.2,
                'waist' => 75,
                'thigh' => 52,
                'created_at' => new DateTime('2018-08-08')
            ],
            [
                'user_id' => 4,
                'weight' => 60.9,
                'body_fat' => 10.2,
                'height' => 173,
                'biceps' => 30.4,
                'chest' => 90.4,
                'waist' => 75,
                'thigh' => 52.3,
                'created_at' => new DateTime('2018-08-15')
            ],
            [
                'user_id' => 4,
                'weight' => 61.3,
                'body_fat' => 10.3,
                'height' => 173.2,
                'biceps' => 30.6,
                'chest' => 90.6,
                'waist' => 74.9,
                'thigh' => 52.4,
                'created_at' => new DateTime('2018-08-22')
            ],
            [
                'user_id' => 4,
                'weight' => 61.9,
                'body_fat' => 10.3,
                'height' => 173.3,
                'biceps' => 30.9,
                'chest' => 91,
                'waist' => 74.8,
                'thigh' => 52.9,
                'created_at' => new DateTime('2018-08-29')
            ],
            [
                'user_id' => 4,
                'weight' => 62.4,
                'body_fat' => 10.5,
                'height' => 173.3,
                'biceps' => 31.2,
                'chest' => 91.3,
                'waist' => 74.9,
                'thigh' => 53,
                'created_at' => new DateTime('2018-09-07')
            ],
            [
                'user_id' => 4,
                'weight' => 63.8,
                'body_fat' => 10.6,
                'height' => 173.4,
                'biceps' => 31.7,
                'chest' => 91.5,
                'waist' => 75,
                'thigh' => 53.2,
                'created_at' => new DateTime('2018-09-15')
            ],
            [
                'user_id' => 4,
                'weight' => 65.5,
                'body_fat' => 11,
                'height' => 173.5,
                'biceps' => 32.4,
                'chest' => 92.7,
                'waist' => 75,
                'thigh' => 54,
                'created_at' => new DateTime('2018-10-20')
            ],
            [
                'user_id' => 4,
                'weight' => 66.1,
                'body_fat' => 11.4,
                'height' => 173.7,
                'biceps' => 32.7,
                'chest' => 93,
                'waist' => 75.1,
                'thigh' => 54.3,
                'created_at' => new DateTime('2018-10-27')
            ],
            [
                'user_id' => 4,
                'weight' => 66.8,
                'body_fat' => 11.8,
                'height' => 173.8,
                'biceps' => 33,
                'chest' => 93.2,
                'waist' => 75.1,
                'thigh' => 55,
                'created_at' => new DateTime('2018-11-04')
            ],
            [
                'user_id' => 4,
                'weight' => 67.5,
                'body_fat' => 12.1,
                'height' => 173.9,
                'biceps' => 33.2,
                'chest' => 93.6,
                'waist' => 75.3,
                'thigh' => 55.2,
                'created_at' => new DateTime('2018-11-11')
            ],
            [
                'user_id' => 4,
                'weight' => 68,
                'body_fat' => 12.2,
                'height' => 174,
                'biceps' => 33.3,
                'chest' => 93.8,
                'waist' => 75.4,
                'thigh' => 55.4,
                'created_at' => new DateTime('2018-11-20')
            ],

        ]);
        DB::table('products')->insert([
            [
                'name' => 'Pistacje',
                'proteins' => 24.2,
                'carbs' => 18.9,
                'fats' => 48.5,
                'calories' => 621,
                'user_id' => 4,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Orzechy ziemne',
                'proteins' => 26.6,
                'carbs' => 14.4,
                'fats' => 48,
                'calories' => 610,
                'user_id' => 4,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => null
            ]
        ]);
    }
}
