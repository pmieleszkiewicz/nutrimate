<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\Product::class, 50)->create();

        DB::table('products')->insert([
            [
                'name' => 'Banan',
                'proteins' => 1,
                'carbs' => 20.2,
                'fats' => 0.3,
                'calories' => 90,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Jabłko',
                'proteins' => 0.2,
                'carbs' => 11.4,
                'fats' => 0.1,
                'calories' => 50,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Gruszka',
                'proteins' => 0.4,
                'carbs' => 12.4,
                'fats' => 0.1,
                'calories' => 58,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Śliwka węgierka',
                'proteins' => 0.7,
                'carbs' => 11.4,
                'fats' => 0.0,
                'calories' => 46,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Pomarańcza',
                'proteins' => 0.9,
                'carbs' => 9.5,
                'fats' => 0.2,
                'calories' => 43,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Pomarańcza',
                'proteins' => 0.9,
                'carbs' => 9.5,
                'fats' => 0.2,
                'calories' => 43,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Truskawki',
                'proteins' => 0.7,
                'carbs' => 5.5,
                'fats' => 0.4,
                'calories' => 28,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Wiśnie',
                'proteins' => 0.9,
                'carbs' => 10,
                'fats' => 0.4,
                'calories' => 47,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Winogrona',
                'proteins' => 0.5,
                'carbs' => 16.2,
                'fats' => 0.2,
                'calories' => 68,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Ziemniaki surowe',
                'proteins' => 1.5,
                'carbs' => 13.2,
                'fats' => 0.2,
                'calories' => 68,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Bataty',
                'proteins' => 1.6,
                'carbs' => 20.1,
                'fats' => 0.1,
                'calories' => 88,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Marchew',
                'proteins' => 0.7,
                'carbs' => 3.8,
                'fats' => 0.1,
                'calories' => 19,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Fasola biała',
                'proteins' => 21.4,
                'carbs' => 46.0,
                'fats' => 1.6,
                'calories' => 19,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Ogórek',
                'proteins' => 0.5,
                'carbs' => 1.8,
                'fats' => 0.1,
                'calories' => 10,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Pomidor',
                'proteins' => 0.9,
                'carbs' => 2.4,
                'fats' => 0.2,
                'calories' => 15,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],

            [
                'name' => 'Pierś z kurczaka surowa, bez skóry',
                'proteins' => 21.8,
                'carbs' => 0,
                'fats' => 3.7,
                'calories' => 121,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Szynka',
                'proteins' => 17,
                'carbs' => 0,
                'fats' => 3.0,
                'calories' => 96,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Pierś z indyka surowa, bez skóry',
                'proteins' => 17.1,
                'carbs' => 4.2,
                'fats' => 1.7,
                'calories' => 104,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Pierś z indyka surowa, bez skóry',
                'proteins' => 17.1,
                'carbs' => 4.2,
                'fats' => 1.7,
                'calories' => 104,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Kiełbasa krakowska sucha',
                'proteins' => 25.6,
                'carbs' => 0,
                'fats' => 24.8,
                'calories' => 326,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Schab surowy',
                'proteins' => 19,
                'carbs' => 0,
                'fats' => 4,
                'calories' => 136,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Mięso wołowe mielone',
                'proteins' => 19,
                'carbs' => 0,
                'fats' => 18,
                'calories' => 136,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Salami',
                'proteins' => 22,
                'carbs' => 0.3,
                'fats' => 41,
                'calories' => 459,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],

            // nabiał
            [
                'name' => 'Mleko 2% tłuszczu',
                'proteins' => 3.3,
                'carbs' => 4.8,
                'fats' => 2,
                'calories' => 50,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Jajo kurze',
                'proteins' => 12.5,
                'carbs' => 0,
                'fats' => 9.5,
                'calories' => 143,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Śmietana 12%',
                'proteins' => 3.4,
                'carbs' => 4.3,
                'fats' => 12,
                'calories' => 134,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Śmietana 18%',
                'proteins' => 3.4,
                'carbs' => 4.3,
                'fats' => 18,
                'calories' => 180,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Masło 83%',
                'proteins' => 0,
                'carbs' => 0,
                'fats' => 83,
                'calories' => 753,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Serek wiejski',
                'proteins' => 11.8,
                'carbs' => 2.7,
                'fats' => 5,
                'calories' => 97,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Twaróg chudy',
                'proteins' => 18.3,
                'carbs' => 3.5,
                'fats' => 0.2,
                'calories' => 86,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Twaróg półtłusty',
                'proteins' => 17,
                'carbs' => 3.5,
                'fats' => 5,
                'calories' => 100,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Ser żółty Gouda',
                'proteins' => 25,
                'carbs' => 2.2,
                'fats' => 27,
                'calories' => 356,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Jogurt naturalny',
                'proteins' => 4,
                'carbs' => 5.9,
                'fats' => 2,
                'calories' => 58,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Serek topiony',
                'proteins' => 16,
                'carbs' => 16,
                'fats' => 10,
                'calories' => 215,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],

            // pieczywo
            [
                'name' => 'Bułka grahamka',
                'proteins' => 8.5,
                'carbs' => 56.8,
                'fats' => 1.7,
                'calories' => 270,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Bułka kajzerka',
                'proteins' => 9.9,
                'carbs' => 52.7,
                'fats' => 4.3,
                'calories' => 293,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Chleb pszenny',
                'proteins' => 8.5,
                'carbs' => 54.3,
                'fats' => 1.4,
                'calories' => 262,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Chleb żytni',
                'proteins' => 6,
                'carbs' => 40,
                'fats' => 1,
                'calories' => 204,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Wafle ryżowe',
                'proteins' => 8,
                'carbs' => 81,
                'fats' => 2.9,
                'calories' => 374,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Chleb tostowy pszenny',
                'proteins' => 9,
                'carbs' => 54.4,
                'fats' => 4,
                'calories' => 293,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],

            // Ryby i owoce morza
            [
                'name' => 'Dorsz świeży',
                'proteins' => 15.3,
                'carbs' => 0,
                'fats' => 0.4,
                'calories' => 60,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Łosoś świeży',
                'proteins' => 20.5,
                'carbs' => 0,
                'fats' => 4.4,
                'calories' => 127,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Tuńczyk w oleju',
                'proteins' => 24.6,
                'carbs' => 0.5,
                'fats' => 2.8,
                'calories' => 124,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Tuńczyk w sosie własnym',
                'proteins' => 24.6,
                'carbs' => 0.5,
                'fats' => 0.4,
                'calories' => 124,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Śledź w oleju',
                'proteins' => 16.4,
                'carbs' => 0,
                'fats' => 26.5,
                'calories' => 304,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Pstrąg',
                'proteins' => 19.9,
                'carbs' => 0,
                'fats' => 6.2,
                'calories' => 141,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Krewetki',
                'proteins' => 20.6,
                'carbs' => 0.9,
                'fats' => 2,
                'calories' => 106,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],

            // Produkty zbożowe
            [
                'name' => 'Płatki owsiane',
                'proteins' => 16.9,
                'carbs' => 66.3,
                'fats' => 6.9,
                'calories' => 389,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Kasza jaglana',
                'proteins' => 11,
                'carbs' => 72.9,
                'fats' => 4.2,
                'calories' => 378,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Makaron pełnoziarnisty',
                'proteins' => 12,
                'carbs' => 66,
                'fats' => 3,
                'calories' => 350,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Ryż brązowy',
                'proteins' => 7.7,
                'carbs' => 72,
                'fats' => 3.2,
                'calories' => 354,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Mąka pszenna',
                'proteins' => 10.3,
                'carbs' => 76.3,
                'fats' => 1,
                'calories' => 364,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Musli owocowe',
                'proteins' => 8.5,
                'carbs' => 71.8,
                'fats' => 5,
                'calories' => 367,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Ryż biały',
                'proteins' => 8,
                'carbs' => 78.5,
                'fats' => 1,
                'calories' => 370,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Płatki kukurydziane',
                'proteins' => 7.7,
                'carbs' => 88.5,
                'fats' => 0.3,
                'calories' => 399,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Makaron pszenny',
                'proteins' => 12,
                'carbs' => 77.5,
                'fats' => 2,
                'calories' => 370,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],

            // Odżywki
            [
                'name' => 'Odżywka białkowa WPC80',
                'proteins' => 78,
                'carbs' => 14.3,
                'fats' => 1,
                'calories' => 391,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Odżywka gainer',
                'proteins' => 20,
                'carbs' => 80,
                'fats' => 0,
                'calories' => 378,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],

            // Frytki
            [
                'name' => 'Frytki',
                'proteins' => 3.8,
                'carbs' => 38,
                'fats' => 16,
                'calories' => 314,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Pizza',
                'proteins' => 9.8,
                'carbs' => 40.2,
                'fats' => 2.5,
                'calories' => 223,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Popcorn',
                'proteins' => 7.5,
                'carbs' => 51.5,
                'fats' => 27,
                'calories' => 480,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Kebab w bułce',
                'proteins' => 8.2,
                'carbs' => 19.8,
                'fats' => 9.7,
                'calories' => 195,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],

            [
                'name' => 'Oliwa z oliwek',
                'proteins' => 0,
                'carbs' => 0.2,
                'fats' => 99.6,
                'calories' => 897,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
            [
                'name' => 'Olej rzepakowy',
                'proteins' => 0,
                'carbs' => 0.2,
                'fats' => 99.6,
                'calories' => 897,
                'user_id' => 1,
                'created_at' =>  new \DateTime(),
                'proposed_at' => new \DateTime(),
                'accepted_by' => 1
            ],
        ]);

    }
}
