<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
//use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    const ROLES = ['admin', 'moderator', 'user'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::ROLES as $roleName) {
            $role = Role::create(['name' => $roleName]);
            $role->save();
        }
    }
}
