<?php

use App\Settings;
use App\User;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        /* if user is male then set blue theme, if female pink one */
        foreach ($users as $user) {
            $settings = Settings::create([
                'theme' => $user->isMale() ? false : true
            ]);
            $user->settings()->save($settings);
        }
    }
}
