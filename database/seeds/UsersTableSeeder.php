<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Paweł',
                'email' => 'pawelmieleszkiewicz@gmail.com',
                'password' => bcrypt('Pawel!23'),
                'date_of_birth' => new DateTime('29-08-1995'),
                'gender' => 'm',
                'role_id' => 1,
                'created_at' => new DateTime(),
                'api_token' => str_random(64)
            ],
            [
                'name' => 'Administrator',
                'email' => 'admin@nutrimate.com',
                'password' => bcrypt('admin123'),
                'date_of_birth' => new DateTime('01-01-2000'),
                'gender' => 'm',
                'role_id' => 1,
                'created_at' => new DateTime(),
                'api_token' => str_random(64)
            ],
            [
                'name' => 'Moderator',
                'email' => 'support@nutrimate.com',
                'password' => bcrypt('moderator123'),
                'date_of_birth' => new DateTime('01-01-1990'),
                'gender' => 'm',
                'role_id' => 2,
                'created_at' => new DateTime(),
                'api_token' => str_random(64)
            ],
            [
                'name' => 'User',
                'email' => 'user@nutrimate.com',
                'password' => bcrypt('user123'),
                'date_of_birth' => new DateTime('01-08-1990'),
                'gender' => 'm',
                'role_id' => 3,
                'created_at' => new DateTime(),
                'api_token' => str_random(64)
            ],
            [
                'name' => 'Monia',
                'email' => 'monia.szczawinska@gmail.com',
                'password' => bcrypt('rokijestsuper'),
                'date_of_birth' => new DateTime('01-01-1996'),
                'gender' => 'f',
                'role_id' => 1,
                'created_at' => new DateTime(),
                'api_token' => str_random(64)
            ],
        ]);

        factory(App\User::class, 20)->create();
    }
}
