const URL = '/api'
// const URL = 'http://pmieleszkiewicz-nutrimate.herokuapp.com/api'

export default {
    GET_USER_MEALS: URL + '/meals',
    DELETE_USER_MEAL: URL + '/meals',
    SAVE_USER_MEAL: URL + '/meals',
    MEALS: URL + '/meals',
    USERS: URL + '/users',
    MEASUREMENTS: URL + '/measurements',

    PRODUCTS: URL + '/products',

    HEADERS: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
    }
}