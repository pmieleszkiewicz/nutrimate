
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import Vuex from 'vuex';
import router from './router'
import storeData from './store'

import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import datePicker from 'vue-bootstrap-datetimepicker';


Vue.use(VueRouter)
Vue.use(BootstrapVue);
Vue.use(Vuex);
Vue.use(VueChartkick, {adapter: Chart})
Vue.use(datePicker);

const store = new Vuex.Store(storeData);

// router.beforeEach((to, from, next) => {
//     if (store.getters.user) {
//         if (to.name !== 'profile' && !store.getters.user.goal) {
//             next({ path: 'profile' });
//             return;
//         }
//         next();
//         return;
//     }
//     next();
// });

Vue.component('app', require('./components/App.vue'));

// Register a global custom directive called v-focus
Vue.directive('focus', {
    // When the bound element is inserted into the DOM...
    inserted: function (el) {
        // Focus the element
        el.focus()
    }
})

const app = new Vue({
    el: '#app',
    router,
    store
});
