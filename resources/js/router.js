import Vue from 'vue'
import Router from 'vue-router'

import Dashboard from './components/pages/Dashboard';
import Charts from './components/pages/Charts';
import Profile from './components/pages/Profile';
import DailySummary from './components/pages/DailySummary';
import MyProducts from './components/pages/MyProducts';
import MyProductDetails from './components/my-products/MyProductsDetails';
import Measurements from './components/pages/Measurements';
import MeasurementDetails from './components/measurements/MeasurementDetails';

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: '/app/',
    routes: [
        {
            path: '',
            name: 'dashboard',
            component: Dashboard
        },
        {
            path: '/charts',
            name: 'charts',
            component: Charts
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/daily-summary',
            name: 'dailySummary',
            component: DailySummary
        },
        {
            path: '/my-products',
            name: 'myProducts',
            component: MyProducts
        },
        {
            path: '/my-products/:id',
            name: 'myProductsDetails',
            component: MyProductDetails
        },
        {
            path: '/measurements',
            name: 'measurements',
            component: Measurements
        },
        {
            path: '/measurements/:id',
            name: 'measurementDetails',
            component: MeasurementDetails
        },
    ],
})
