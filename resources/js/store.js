import API from './api'

export default {
  state: {
      token: '',
      user: null,
      settings: {},
      meals: null,
      daySummary: null,
      summaryDate: null
  },
  getters: {
      user(state) {
          return state.user;
      },
      meals(state) {
          return state.meals;
      },
      daySummary(state) {
          return state.daySummary;
      },
      settings(state) {
          return state.settings;
      },
      /* settings getters */
      theme(state) {
          return state.settings.theme ? 'pink' : 'primary';
      },
      themeRGB(state, getters) {
          return getters.theme === 'pink' ? '#d61278' : '#2c3c76';
      },
      button(state) {
          return {
              btn: true,
              'btn-primary': !state.settings.theme,
              'btn-pink': state.settings.theme
          };
      },

      getSummaryDate(state) {
          return state.summaryDate;
      }
  },
  mutations: {
      setUser(state, payload) {
          state.user = payload;
          state.token = payload.api_token;
          state.settings = payload.settings;
      },
      calculateDaySummary(state, payload) {
          let daySummary = {
              proteins: 0,
              carbs: 0,
              fats: 0,
              calories: 0,
          };
          state.meals.forEach(function (meal) {
              meal.products.forEach(function (product) {
                  daySummary.proteins += product.proteins * product.pivot.weight / 100;
                  daySummary.carbs+= product.carbs * product.pivot.weight / 100;
                  daySummary.fats += product.fats* product.pivot.weight / 100;
                  daySummary.calories += product.calories * product.pivot.weight / 100;
              });
          });
          daySummary.proteins = Math.round(daySummary.proteins * 10) / 10;
          daySummary.carbs = Math.round(daySummary.carbs * 10) / 10;
          daySummary.fats = Math.round(daySummary.fats * 10) / 10;
          daySummary.calories = Math.round(daySummary.calories);

          state.daySummary = daySummary;
      },
      setUserMeals(state, payload) {
          state.meals = payload;
      },
      addNewMeal(state, payload) {
          state.meals.push(payload);
      },
      deleteUserMeal(state, payload) {
          let index = state.meals.findIndex(m => m.id == payload.id);
          Vue.delete(state.meals, index);
      },
      updateUserMeal(state, payload) {
          let index = state.meals.findIndex(m => m.id == payload.id);
          Vue.set(state.meals, index, payload);
      },
      updateUserMealName(state, payload) {
          let index = state.meals.findIndex(m => m.id == payload.id);
          Vue.set(state.meals, index, payload);
      },

      setSummaryDate(state, payload) {
          state.summaryDate = payload;
      }
  },
  actions: {
      getUserMeals(context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'GET',
                  url: API.GET_USER_MEALS,
                  params: {
                      date: payload.toLocaleString()
                  }
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          context.commit('setUserMeals', response.data);
                          context.commit('calculateDaySummary');
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      deleteUserMeal(context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'DELETE',
                  url: API.DELETE_USER_MEAL + '/' + payload.id,
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          context.commit('deleteUserMeal', payload);
                          context.commit('calculateDaySummary', payload);
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      /* saves meal with products */
      saveUserMeal(context, payload) {
          let products = payload.products.map(function (product) {
              return {
                  'id': product.id,
                  'weight': product.pivot.weight
              }
          });
          return new Promise((resolve, reject) => {
              axios({
                  method: 'PATCH',
                  url: API.SAVE_USER_MEAL + '/' + payload.id,
                  data: {
                      products: products
                  }
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          context.commit('updateUserMeal', response.data);
                          context.commit('calculateDaySummary');
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      /* creates a new meal */
      addNewMeal(context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'POST',
                  url: API.MEALS,
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          context.commit('addNewMeal', response.data);
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      /* changes meal name */
      editUserMeal(context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'PATCH',
                  url: API.SAVE_USER_MEAL + '/' + payload.meal.id,
                  data: {
                      mealName: payload.newName
                  }
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          context.commit('updateUserMealName', response.data);
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },

      /* Settings */
      /* Saves user settings */
      saveSettings: function (context, payload) {
          console.log(payload)
          return new Promise((resolve, reject) => {
              axios({
                  method: 'PUT',
                  url: API.USERS + '/settings',
                  data: {
                      theme: payload.theme,
                      show_numbers_in_bars: payload.show_numbers_in_bars,
                      number_of_items_in_table: payload.number_of_items_in_table
                  }
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          // context.commit('updateUserMealName', response.data);
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },

      /* My products */
      /* Get user's products */
      getUserProducts: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'GET',
                  url: API.USERS + '/products',
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          // context.commit('updateUserMealName', response.data);
                          const products = response.data.map((product) => {
                              return {
                                  ...product,
                                  proteins: product.proteins.toFixed(1),
                                  carbs: product.carbs.toFixed(1),
                                  fats: product.fats.toFixed(1)
                              }
                          });
                          resolve(products);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },

      deleteProduct: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'DELETE',
                  url: API.PRODUCTS + '/' + payload.id,
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },

      getProduct: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'GET',
                  url: API.PRODUCTS + '/' + payload,
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      updateProduct: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'PATCH',
                  url: API.PRODUCTS + '/' + payload.id,
                  data: payload
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      saveProduct: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'POST',
                  url: API.PRODUCTS,
                  data: payload
              })
                  .then(function (response) {
                      if (response.status === 201) {
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      proposeProduct: function (context, payload) {
          return new Promise((resolve, reject) => {
              console.log(payload)
              axios({
                  method: 'PATCH',
                  url: API.PRODUCTS + '/' + payload + '/propose',
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },

      getMeasurements: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'GET',
                  url: API.MEASUREMENTS,
                  params: {
                      startDate: payload
                  }
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          // context.commit('updateUserMealName', response.data);
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },

      getMeasurement: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'GET',
                  url: API.MEASUREMENTS + `/${payload}`,
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      deleteMeasurement: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'DELETE',
                  url: API.MEASUREMENTS + '/' + payload.id,
              })
                  .then(function (response) {
                      if (response.status === 200) {
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      saveMeasurement: function (context, payload) {
          return new Promise((resolve, reject) => {
              axios({
                  method: 'POST',
                  url: API.MEASUREMENTS,
                  data: payload
              })
                  .then(function (response) {
                      if (response.status === 201) {
                          resolve(response);
                      }
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },
      saveGoal: function (context, payload) {
          console.log(payload.userId)
          console.log(payload.goal)
          return new Promise((resolve, reject) => {
              axios({
                  method: 'POST',
                  url: `${API.USERS}/${payload.userId}/goal`,
                  data: payload.goal
              })
                  .then(function (response) {
                      resolve(response);
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          })
      },

  }
};