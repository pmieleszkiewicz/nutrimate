<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasło musi się składać z minimum 6 znaków i musi być takie same.',
    'reset' => 'Twoje hasło zostało zresetowane!',
    'sent' => 'Link resetujący hasło został wysłany!',
    'token' => 'Link resetujący hasło jest niepoprawny.',
    'user' => "Nie ma takiego użytkownika.",

];
