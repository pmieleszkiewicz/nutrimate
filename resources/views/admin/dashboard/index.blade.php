@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card text-white bg-success mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-user-friends"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $usersNumber }}</span>
                        <span class="text-tile-sm">użytkowników</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card text-white bg-danger mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-utensils"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $productsNumber }}</span>
                        <span class="text-tile-sm">produktów</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card text-white bg-info mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-weight"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $measurementsNumber }}</span>
                        <span class="text-tile-sm">pomiarów</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col-md-6 mt-2">
            <h4 class="text-muted text-center">Najnowsi użytkownicy</h4>
            <div class="card">
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @foreach($users as $user)
                            <li class="list-group-item">
                                <i class="fas fa-{{ $user->isMale() ? 'mars' : 'venus' }} text-tile-sm text-{{ $user->isMale() ? 'info' : 'pink' }}"></i>
                                <a href="{{ route('admin.users.show', ['id' => $user->id]) }}">{{ $user->name }}</a>
                                <span class="float-right">{{ $user->created_at->diffForHumans() }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-md-6 mt-2">
            <h4 class="text-muted text-center">Najnowsze produkty</h4>
            <div class="card">
                <div class="card-body">
                    <ul class="list-group list-group-flush text-right">
                        @foreach($products as $product)
                            <li class="list-group-item">
                                <span class="float-left">
                                    <a href="{{ route('admin.products.show', ['id' => $product->id]) }}">{{ $product->name }}</a>
                                </span>
                                <span>{{ $product->proposed_at->diffForHumans() }}</span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col">
            <h4 class="text-muted text-center">Ostatnio zalogowani</h4>
            <div class="card">
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @foreach($lastLoggedUsers as $user)
                            <li class="list-group-item">
                                @if($user->role->name === 'admin')
                                    <span class="badge badge-danger">{{ $user->role->name }}</span>
                                @elseif($user->role->name === 'moderator')
                                    <span class="badge badge-info">{{ $user->role->name }}</span>
                                @else
                                    <span class="badge badge-primary">{{ $user->role->name }}</span>
                                @endif
                                <a href="{{ route('admin.users.show', ['id' => $user->id]) }}">{{ $user->name }}</a>
                                <span class="float-right">
                                    <small>({{ $user->last_login_at->diffForHumans() }})</small>
                                    {{ $user->last_login_at->format('d-m-Y H:i') }}
                                </span>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
