@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::has('status'))
        <p class="alert alert-success">{{ Session::get('status') }}</p>
    @endif

    <div class="card">
        <div class="card-body">
            <h1>{{ $product->name }}</h1>

            <form method="POST" action="{{ route('admin.products.update', ['id' => $product->id]) }}">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="productName">Nazwa produktu</label>
                    <input type="text" class="form-control" id="productName" name="name" value="{{ $product->name }}">
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="productProteins">Białko</label>
                        <input type="text" class="form-control" id="productProteins" name="proteins" value="{{ $product->proteins }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="productCarbs">Węglowodany</label>
                        <input type="text" class="form-control" id="productCarbs" name="carbs" value="{{ $product->carbs }}">
                    </div>
                </div>
                <div class="form-row">
                    {{--<div class="form-group col-md-6">--}}
                        {{--<label for="productSugars">Cukry</label>--}}
                        {{--<input type="text" class="form-control" id="productSugars" name="sugars" value="{{ $product->sugars }}">--}}
                    {{--</div>--}}
                    <div class="form-group col-md-6">
                        <label for="productFats">Tłuszcz</label>
                        <input type="text" class="form-control" id="productFats" name="fats" value="{{ $product->fats }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="productCalories">Kalorie</label>
                    <input type="text" class="form-control" id="productCalories" name="calories" value="{{ $product->calories }}">
                </div>

                <div class="form-check">
                    <input class="form-check-input" value="test" type="checkbox" name="isGlobal" id="productGlobal" {{ $product->accepted_by ? 'checked' : '' }}>
                    <label class="form-check-label" for="productGlobal">
                        Produkt globalny
                    </label>
                </div>

                <div class="float-right">
                    <a href="{{ route('admin.products.show', ['id' => $product->id]) }}" class="btn btn-link">Anuluj</a>
                    <button type="submit" class="btn btn-{{ Auth::user()->getThemeName() }}">Zapisz</button>
                </div>
            </form>

        </div>
    </div>
@endsection
