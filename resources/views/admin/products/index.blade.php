@extends('layouts.app')

@section('content')

    {{--<div class="alert alert-info" role="alert">--}}
        {{--<p>Wyszukiwanie dozwolone po: <strong>ID</strong>, <strong>nazwa</strong><br>--}}
            {{--Aby zresetować filtry należy wysłac pusty ciąg znaków.--}}
        {{--</p>--}}
        {{--<p>--}}
            {{--<strong>Produkty globalne</strong> są dostępne dla każdego użytkownika<br />--}}
            {{--<strong>Produkty zaproponowane</strong> to produkty prywatne, którymi użytkownik chce się podzielić z innymi.--}}
            {{--Muszą być one zaakceptowane przez <strong>admina</strong> lub <strong>moderatora</strong>--}}
        {{--</p>--}}
    {{--</div>--}}

    <form method="GET" action="{{ route('admin.products.index') }}" class="mb-2">
        <div class="form-row">
            <div class="col-8 col-md-9 col-lg-10">
                <input type="text" class="form-control" name="search" placeholder="Szukaj..." value="{{ Request::get('search') }}">
            </div>
            <div class="col-4 col-md-3 col-lg-2">
                <button class="btn btn-{{ Auth::user()->getThemeName() }} btn-block" type="submit"><i class="fas fa-search"></i> Szukaj</button>
            </div>
        </div>
        @if(Request::get('proposed'))
            <input type="hidden" name="proposed" value="1">
        @endif
    </form>

    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.products.index') && !Request::get('proposed') ? 'active' : '' }}"
                       href="{{ route('admin.products.index') }}">
                        Produkty globalne
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('admin.products.index') && Request::get('proposed') ? 'active' : '' }}"
                       href="{{ route('admin.products.index', ['proposed' => true]) }}">
                        Produkty zaproponowane
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <table class="table table-responsive-md">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nazwa produktu</th>
                    <th scope="col">Dodany przez</th>
                    <th scope="col">Dodany dnia</th>
                    @if(!Request::get('proposed'))
                        <th scope="col">Zaakceptowany przez</th>
                    @endif
                    <th scope="col" class="text-right">Opcje</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    <tr>
                        <th scope="row">{{ $product->id }}</th>
                        <td>{{ $product->name }}</td>
                        <td>
                            <a href="{{ route('admin.users.show', ['id' => $product->creator->id]) }}">
                                {{ $product->creator->name }}
                            </a>
                        </td>
                        <td>{{ $product->proposed_at }}</td>
                        @if($product->accepted_by)
                            <td>
                                <a href="{{ route('admin.users.show', ['id' => $product->userWhoAccepted->id]) }}">
                                    {{ $product->userWhoAccepted->name }}
                                </a>
                            </td>
                        @endif
                        <td class="text-right">
                            {{--<a href="{{ route('admin.products.edit', ['id' => $product->id]) }}" class="btn btn-dark btn-sm">--}}
                                {{--<i class="fas fa-edit"></i> Edit--}}
                            {{--</a>--}}
                            <a href="{{ route('admin.products.show', ['id' => $product->id]) }}" class="btn btn-{{ Auth::user()->getThemeName() }} btn-sm">
                                <i class="fas fa-info-circle"></i> Szczegóły
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Pagination--}}
    <div class="float-right mt-2">
        {{ $products->onEachSide(1)->links() }}
    </div>

@endsection
