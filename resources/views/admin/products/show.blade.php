@extends('layouts.app')

@section('content')
    @if(Session::has('status'))
        <p class="alert alert-success">{{ Session::get('status') }}</p>
    @endif

    @if($product->proposed_at && !$product->accepted_by)
        <form method="POST" action="{{ route('admin.products.accept', ['id' => $product->id]) }}">
            {{ method_field('PATCH') }}
            {{ csrf_field() }}
            <button type="submit" class="btn btn-success mb-2 btn-block btn-sm">Dodaj jako produkt globalny</button>
        </form>
    @endif

    <div class="card">
        <div class="card-body">
            <h1>
                {{ $product->name }}
                <a href="{{ route('admin.products.edit', ['id' => $product->id]) }}" class="btn btn-{{ Auth::user()->getThemeName() }} float-right">
                    <i class="fas fa-edit"></i> Edytuj
                </a>
            </h1>
            <h4 class="mt-4">Tabela makroskładników</h4>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Białko</th>
                    <th scope="col">Węglowodany</th>
                    {{--<th scope="col" class="text-normal">Cukry</th>--}}
                    <th scope="col">Tłuszcz</th>
                    <th scope="col">Kalorie</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $product->proteins }}</td>
                        <td>{{ $product->carbs }}</td>
{{--                        <td>{{ $product->sugars }}</td>--}}
                        <td>{{ $product->fats }}</td>
                        <td>{{ $product->calories }}</td>
                    </tr>
                </tbody>
            </table>

            <p class="mt-4">
                <strong>Utworzony przez: </strong>
                <a href="{{ route('admin.users.show', ['id' => $product->creator->id]) }}">
                    {{ $product->creator->name }}
                </a>
                , {{ $product->created_at->diffForHumans() }}
            </p>

        </div>
    </div>
@endsection
