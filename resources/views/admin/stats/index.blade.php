@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-4">
            <div class="card text-white bg-success mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-user-friends"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $users }}</span>
                        <span class="text-tile-sm">użytkowników</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card text-white bg-danger mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-utensils"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $products }}</span>
                        <span class="text-tile-sm">produktów</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card text-white bg-info mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-weight"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $measurements }}</span>
                        <span class="text-tile-sm">pomiarów</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="card bg-light mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-user-friends"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $usersLastWeek }}</span>
                        <span class="text-tile-sm">w ostatnim tygodniu</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card bg-light mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-utensils"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $productsLastWeek }}</span>
                        <span class="text-tile-sm">w ostatnim tygodniu</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card bg-light mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-weight"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $measurementsLastWeek }}</span>
                        <span class="text-tile-sm">w ostatnim tygodniu</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4">
            <div class="card bg-light mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-user-friends"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $usersLastMonth }}</span>
                        <span class="text-tile-sm">w ostatnim miesiącu</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card bg-light mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-utensils"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $productsByUsers }}</span>
                        <span class="text-tile-sm">przez użytkowników</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card bg-light mb-3 shadow-sm">
                <div class="card-body d-flex align-items-center justify-content-around">
                    <div class="text-tile-lg"><i class="fas fa-weight"></i></div>
                    <div class="font-weight-bold text-center">
                        <span class="text-tile-md block">{{ $measurementsLastMonth }}</span>
                        <span class="text-tile-sm">w ostatnim miesiącu</span>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
