<p class="card-text">
    <table class="table table-responsive-sm">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Nazwa produktu</th>
            <th scope="col">Dodany dnia</th>
            <th scope="col">Zaakceptowany przez</th>
            <th scope="col" class="text-right">Opcje</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">{{ $product->id }}</th>
                <td>{{ $product->name }}</td>
                <td>{{ $product->proposed_at }}</td>
                <td>
                    @if($product->accepted_by)
                        <a href="{{ route('admin.users.show', ['id' => $product->userWhoAccepted->id]) }}">
                            {{ $product->userWhoAccepted->name }}
                        </a>
                    @endif
                </td>
                <td class="text-right">
                    <a href="{{ route('admin.products.show', ['id' => $product->id]) }}" class="btn btn-{{ Auth::user()->getThemeName() }} btn-sm">
                        <i class="fas fa-info-circle"></i> Szczegóły
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</p>