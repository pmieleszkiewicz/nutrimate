<p class="card-text">
    {{--<img src="http://www.taxtoken.in/wp-content/uploads/2017/11/300X300-2.png" class="rounded" width="150px" height="150px">--}}

    <p>
        @if($user->role->name === 'admin')
            <span class="badge badge-danger">{{ $user->role->name }}</span>
        @elseif($user->role->name === 'moderator')
            <span class="badge badge-info">{{ $user->role->name }}</span>
        @else
            <span class="badge badge-primary">{{ $user->role->name }}</span>
        @endif

        <span class="badge badge-{{ $user->blocked ? 'danger' : 'success' }}">
            {{ $user->blocked ? "zablokowany {$user->blocked_at}" : ($user->gender == 'm' ? 'aktywny' : 'aktywna') }}
        </span>
    </p>
    <p><strong>Nazwa użytkownika: </strong>
        {{ $user->name }}
        <i class="fas fa-{{ $user->gender == 'm' ? 'mars' : 'venus' }}"></i>
    </p>
    <p><strong>Email:</strong> {{ $user->email }}</p>
    <p><strong>Zarejestrowany:</strong> {{ $user->created_at }}</p>
    <p><strong>Użytkownik od:</strong> {{ $user->created_at->diffForHumans() }}</p>
    @if($user->updated_at)
        <p><strong>Modyfikowany:</strong> {{ $user->updated_at->format('d/m/Y H:m') }} ({{ $user->updated_at->diffForHumans() }})</p>
    @endif

    @if(Auth::user()->role->name === 'admin')
    <form method="POST" action="{{ route('admin.users.update', ['id' => $user->id]) }}" id="myForm">
        {{ method_field('patch') }}
        {{ csrf_field() }}
        <button type="button" class="btn btn-{{ $user->blocked ? 'success' : 'danger' }}" data-toggle="modal" data-target="#exampleModalCenter">
            {{ $user->blocked ? 'Odblokuj' : 'Zablokuj' }}
        </button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">{{ $user->blocked ? 'Odblokuj użytkownika' : 'Zablokuj użytkownika' }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{ $user->blocked ? 'Odblokować' : 'Zablokować' }} użytkownika. Jesteś {{ Auth::user()->gender == 'm' ? 'pewien' : 'pewna' }}?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Anuluj</button>
                    <button type="submit" class="btn btn-primary">Potwierdź</button>
                </div>
            </div>
        </div>
        </div>
    </form>
    @endif
</p>
