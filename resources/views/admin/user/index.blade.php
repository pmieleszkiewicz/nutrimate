@extends('layouts.app')

@section('content')
    <div class="alert alert-info" role="alert">
        <p>Wyszukiwanie dozwolone po: <strong>ID</strong>, <strong>nazwa</strong>, <strong>nazwa roli (admin, staff, user)</strong> i <strong>email</strong></p>
        <p>Aby zresetować filtry należy wysłac pusty ciąg znaków.</p>
    </div>

    <form method="GET" action="{{ route('admin.users.index') }}">
        <div class="form-row">
            <div class="col-8 col-md-9 col-lg-10">
                <input type="text" class="form-control" name="search" placeholder="Szukaj..." value="{{ Request::get('search') }}">
            </div>
            <div class="col-4 col-md-3 col-lg-2">
                <button class="btn btn-{{ Auth::user()->getThemeName() }} btn-block" type="submit"><i class="fas fa-search"></i> Szukaj</button>
            </div>
        </div>
    </form>

    <div class="card mt-2">
        <div class="card-body">
            <table class="table table-responsive-md">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nazwa użytkownika</th>
                        <th scope="col">Rola</th>
                        <th scope="col">Email</th>
                        <th scope="col" class="text-right">Opcje</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <th scope="row">{{ $user->id }}</th>
                        <td>{{ $user->name }}</td>
                        <td>
                            @if($user->role->name === 'admin')
                                <span class="badge badge-danger">{{ $user->role->name }}</span>
                            @elseif($user->role->name === 'moderator')
                                <span class="badge badge-info">{{ $user->role->name }}</span>
                            @else
                                <span class="badge badge-primary">{{ $user->role->name }}</span>
                            @endif
                        </td>
                        <td>{{ $user->email }}</td>
                        <td class="text-right">
                            <a href="{{ route('admin.users.show', ['id' => $user->id]) }}" class="btn btn-{{ Auth::user()->getThemeName() }} btn-sm mx-1">
                                <i class="fas fa-info-circle"></i> Szczegóły
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    {{--Pagination--}}
    <div class="float-right mt-2">
        {{ $users->onEachSide(1)->links() }}
    </div>

@endsection
