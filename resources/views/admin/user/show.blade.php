@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a class="nav-link {{ Route::currentRouteName() === 'admin.users.show' ? 'active' : '' }}"
                       href="{{ route('admin.users.show', ['id' => $user->id]) }}">
                        Informacje
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Route::currentRouteName() === 'admin.users.products' ? 'active' : '' }}"
                       href="{{ route('admin.users.products', ['id' => $user->id]) }}">
                        Zaakceptowane produkty
                    </a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @switch(Route::currentRouteName())
                @case('admin.users.show')
                    @include('admin.user.includes.show-profile')
                @break

                @case('admin.users.products')
                    @include('admin.user.includes.show-products')
                @break

                @default
                Default case...
            @endswitch
        </div>
    </div>

    {{--Pagination only for 'Added products' tab--}}
    @if (Route::is('admin.users.products'))
        <div class="float-right mt-2">
            {{ $products->onEachSide(1)->links() }}
        </div>
    @endif
@endsection
