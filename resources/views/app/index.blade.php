@extends('layouts.app')

@section('content')
    {{--Vue.js entry point--}}
    @php
        $user = Auth::user()->load(['settings', 'goal'])
    @endphp
    <div id="app">
        <App :user="{{ $user }}" />
    </div>


@endsection
