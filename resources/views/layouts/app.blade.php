<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ route(Route::is('admin*') ? 'admin.dashboard' : 'app') }}">
                    @guest
                        <img src="https://www.bankfotek.pl/image/2123307.jpeg" height="25" alt="Website logo">
                    @endguest
                    @auth
                        @if(Auth::user()->getThemeName() === 'primary')
                            <img src="https://www.bankfotek.pl/image/2123307.jpeg" height="25" alt="Website logo">
                        @else
                            <img src="http://www.bankfotek.pl/image/2125223.jpeg" height="25" alt="Website logo">
                        @endif
                    @endauth
                    {{--<img src="{{ URL::to('/') }}/images/logo.png" height="25" alt="Website logo">--}}
{{--                    {{ config('app.name', 'Laravel') }}--}}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @auth
                            @if(Auth::user()->hasRole(['admin', 'moderator']))
                                @if(Route::is('admin*'))
                                    <li class="nav-item">
                                        <a class="nav-link btn btn-sm btn-{{ Auth::user()->getThemeName() }} text-light mt-3 mt-md-0" href="{{ route('app') }}">Przejdź do aplikacji</a>
                                    </li>
                                @else
                                    <li class="nav-item">
                                        <a class="nav-link btn btn-sm btn-{{ Auth::user()->getThemeName() }} text-light mt-3 mt-md-0" href="{{ route('admin.dashboard') }}">Panel administracyjny</a>
                                    </li>
                                @endif
                            @endif
                        @endauth
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Logowanie</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">Zarejestruj się</a>
                            </li>
                        @else
                            <li class="nav-item">
                                {{--<a class="nav-link" href="#">--}}
                                    {{--<img data-src="holder.js/25x25">--}}
                                {{--</a>--}}
                            </li>
                            <li class="nav-item dropdown">

                                <a id="navbarDropdown" class="nav-link dropdown-toggle mt-2 mt-md-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <small>({{ ucwords(Auth::user()->role->name) }})</small>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                    {{--<img class="d-none d-md-inline" data-src="holder.js/40x40">--}}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item dropdown-item{{ Auth::user()->getThemeName(true) }}" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Wyloguj
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <div class="row py-4">
                @if(Route::is('admin*'))
                    @include('layouts.includes.sidebar')
                    <div class="col-md-9">
                        @yield('content')
                    </div>
                @elseif(Route::is('app*'))
                    <div class="col-md-12">
                        @yield('content')
                    </div>
                @else
                    <div class="col-md-10 offset-md-1">
                        @yield('content')
                    </div>
                @endif
                </div>
            </div>
        </div>
    </div>

    @yield('javascripts')
</body>
</html>
