<div class="col-md-3 mb-3">
    <div class="list-group nav-list">
        <span class="text-muted">Ogólne</span>
        <a href="{{ route('admin.dashboard') }}"
           class="list-group-item list-group-item-action {{ Route::is('admin.dashboard*') ? 'active' . Auth::user()->getThemeNameActive() : '' }}">
            <i class="fas fa-tachometer-alt"></i> Panel główny
        </a>
        <a href="{{ route('admin.stats') }}"
           class="list-group-item list-group-item-action {{ Route::is('admin.stats*') ? 'active' . Auth::user()->getThemeNameActive() : '' }}">
            <i class="fas fa-chart-line"></i> Statystyki
        </a>

        <div class="dropdown-divider"></div>
        <span class="text-muted">Baza danych</span>
        <a href="{{ route('admin.users.index') }}"
           class="list-group-item list-group-item-action {{ Route::is('admin.users*') ? 'active' . Auth::user()->getThemeNameActive() : '' }}">
            <i class="fas fa-users"></i> Użytkownicy
        </a>
        <a href="{{ route('admin.products.index') }}"
           class="list-group-item list-group-item-action {{ Route::is('admin.products*') ? 'active' . Auth::user()->getThemeNameActive() : '' }}">
            <i class="fas fa-utensils"></i> Produkty
        </a>

        {{--TODO - product tags--}}
        {{--<a href="#"--}}
           {{--class="list-group-item list-group-item-action {{ Route::is('admin.tags*') ? 'active' : '' }}">--}}
            {{--<i class="fas fa-tags"></i> Product tags--}}
        {{--</a>--}}
    </div>
</div>