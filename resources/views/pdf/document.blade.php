<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">


    <style>
        /*@import url('https://fonts.googleapis.com/css?family=Nunito');*/
        body {
            font-family: 'Nunito', sans-serif;
        }

        h4 {
            font-size: 1.35rem;
        }
    </style>
</head>
<body>
    @foreach($meals as $meal)
        <h4>{{ $meal->name }}</h4>
        <div class="card">
            <div class="card-body">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th scope="col">Product name</th>
                            <th scope="col">Weight (g)</th>
                            <th scope="col">Proteins</th>
                            <th scope="col">Carbohydrates</th>
                            <th scope="col">Fats</th>
                            <th scope="col">Calories</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($meal->products as $product)
                            <tr>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->pivot->weight }}</td>
                                <td>{{ $product->pivot->weight * $product->proteins / 100 }}</td>
                                <td>{{ $product->pivot->weight * $product->carbs / 100 }}</td>
                                <td>{{ $product->pivot->weight * $product->fats / 100 }}</td>
                                <td>{{ $product->pivot->weight * $product->calories }}</td>
                                {{--<td>{{ product.pivot.weight }}</td>--}}
                                {{--<td>{{ (product.pivot.weight * product.proteins / 100).toFixed(1) }}</td>--}}
                                {{--<td>{{ (product.pivot.weight * product.carbs / 100).toFixed(1) }}</td>--}}
                                {{--<td>{{ (product.pivot.weight * product.fats / 100).toFixed(1) }}</td>--}}
                                {{--<td>{{ Math.round(product.pivot.weight * product.calories / 100) }}</td>--}}
                            </tr>
                        @endforeach
                    {{--<tr>--}}
                        {{--<th scope="col">Total</th>--}}
                        {{--<th scope="col">{{ getTotalSummary('weight', meal) }}</th>--}}
                        {{--<th scope="col">{{ getTotalSummary('proteins', meal) }}</th>--}}
                        {{--<th scope="col">{{ getTotalSummary('carbs', meal) }}</th>--}}
                        {{--<th scope="col">{{ getTotalSummary('fats', meal) }}</th>--}}
                        {{--<th scope="col">{{ getTotalSummary('calories', meal) }}</th>--}}
                    {{--</tr>--}}
                    </tbody>
                </table>
            </div>
        </div>
    @endforeach

</body>
</html>
