@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level == 'error')
# @lang('Whoops!')
@else
# @lang('Witaj!')
@endif
@endif

{{-- Intro Lines --}}
{{--@foreach ($introLines as $line)--}}
{{--{{ $line }}--}}

{{--@endforeach--}}


Otrzymałeś tę wiadomość, ponieważ otrzymaliśmy zgłoszenie o zmianie hasła do Twojego konta.

{{-- Action Button --}}

<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
Resetuj hasło
@endcomponent

{{-- Outro Lines --}}
{{--@foreach ($outroLines as $line)--}}
{{--{{ $line }}--}}

{{--@endforeach--}}

Jeżeli nie korzystałeś z funkcji resetowania hasła, zignoruj wiadomość.

{{-- Subcopy --}}
@component('mail::subcopy')
@lang(
    "Jeżeli przycisk nie działa, skopiuj podany link\n".
    'i wklej do paska adresu w przeglądarce: [:actionURL](:actionURL)',
    [
        'actionText' => $actionText,
        'actionURL' => $actionUrl
    ]
)
@endcomponent
@endcomponent
