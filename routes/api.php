<?php

Route::middleware('auth:api')->group(function () {
    Route::apiResource('products', 'ProductController');
    Route::apiResource('meals', 'MealController');
    Route::apiResource('measurements', 'MeasurementController');
    Route::put('users/settings', 'UserController@updateSettings');
    Route::get('users/products', 'UserController@products');
    Route::post('users/{id}/goal', 'GoalController@store');
    Route::patch('products/{id}/propose', 'ProductController@proposeProduct');
});
