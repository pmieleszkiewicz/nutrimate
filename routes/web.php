<?php

use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    if (Auth::check()) {
        return redirect()->route('app');
    }
    return view('welcome');
});

Route::prefix('admin')->name('admin.')->middleware(['auth', 'role:admin|moderator'])->group(function () {
    Route::resource('users', 'Admin\UserController');
    Route::get('users/{id}/products', 'Admin\UserController@products')->name('users.products');
    Route::resource('products', 'Admin\ProductController');
    Route::patch('products/{id}/accept', 'Admin\ProductController@accept')->name('products.accept');
    Route::get('', 'Admin\DashboardController@index')->name('dashboard');
    Route::get('stats', 'Admin\StatsController@index')->name('stats');
});

Auth::routes();

Route::any('app/{query?}', 'AppController@index')->where('query', '.*')
    ->middleware(['auth'])->name('app');