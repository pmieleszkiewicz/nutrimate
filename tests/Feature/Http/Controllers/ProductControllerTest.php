<?php

namespace Tests\Feature\Http\Controllers;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductControllerTest extends TestCase
{
    /**
     * Tests if not authenticated user can create product.
     * @test
     * @return void
     */
    public function can_create_product_not_authorized()
    {
        $response = $this->json('POST', '/api/products');
        $response->assertStatus(401);
    }

    /**
     * Tests if authenticated user can create product when no data passed.
     * @test
     * @return void
     */
    public function can_create_product_authorized_no_data_passed()
    {
        $user = User::find(1);
        $this->actingAs($user, 'api');
        $response = $this->json('POST', '/api/products');
        $response->assertStatus(422);

        $response = $this->json('POST', '/api/products', []);
        $response->assertStatus(422);
    }

    /**
     * Tests if authenticated user can create product when data passed.
     * @test
     * @return void
     */
    public function can_create_product_authorized_data_passed()
    {
        $user = User::find(1);
        $this->actingAs($user, 'api');
        $product = [
            'name' => 'Orzechy włoskie',
            'proteins' => 18,
            'carbs' => 11.5,
            'fats' => 60.3,
            'calories' => 666,
            'user_id' => $user->id,
        ];

        $response = $this->json('POST', '/api/products', $product);
        $response->assertStatus(201);

        $product['proteins'] = -10;
        $response = $this->json('POST', '/api/products', $product);
        $response->assertStatus(422);

        $product['proteins'] = 18;
        $product['calories'] = -100;
        $response = $this->json('POST', '/api/products', $product);
        $response->assertStatus(422);
    }

    /**
     * Tests if authenticated user can create propose product.
     * @test
     * @return void
     */
    public function can_user_propose_product()
    {
        $response = $this->json('PATCH', '/api/products/65/propose');
        $response->assertStatus(401);

        $user = User::find(1);
        $this->actingAs($user, 'api');

        $response = $this->json('PATCH', '/api/products/65/propose');
        $response->assertStatus(404);

        $response = $this->json('PATCH', '/api/products/1');
        $response->assertStatus(200);
    }
}
