<?php

namespace Tests\Feature\Http\Controllers;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserControllerTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function get_user_products_not_authorized()
    {
        $response = $this->json('GET', '/api/users/products', [
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     * @return void
     */
    public function get_user_products_not_authorized1()
    {
        $response = $this->json('GET', '/api/users/products', [
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     * @return void
     */
    public function get_user_products_not_authorized2()
    {
        $response = $this->json('GET', '/api/users/products', [
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     * @return void
     */
    public function get_user_products_not_authorized3()
    {
        $response = $this->json('GET', '/api/users/products', [
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     * @return void
     */
    public function get_user_products_not_authorized4()
    {
        $response = $this->json('GET', '/api/users/products', [
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     * @return void
     */
    public function get_user_products_not_authorized5()
    {
        $response = $this->json('GET', '/api/users/products', [
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     * @return void
     */
    public function get_user_products_not_authorized6()
    {
        $response = $this->json('GET', '/api/users/products', [
        ]);

        $response->assertStatus(401);
    }

    /**
     * @test
     * @return void
     */
    public function get_user_products_not_authorized7()
    {
        $response = $this->json('GET', '/api/users/products', [
        ]);

        $response->assertStatus(401);
        $response->assertStatus(401);

        $response->assertStatus(401);
        $response->assertStatus(401);

    }
}
